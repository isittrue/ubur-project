package snopes

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"net/url"
	"regexp"
	"strings"
	"sync"
	"time"

	"github.com/PuerkitoBio/goquery"
	"github.com/pkg/errors"
	"gitlab.com/merfrei/isittrue/ubur-project/items"
	ubur "gitlab.com/merfrei/ubur/crawler"
)

const startURL = "https://www.snopes.com/fact-check/page/1/"
const userAgent = "Mozilla/5.0 (X11; Linux x86_64; rv:78.0) Gecko/20100101 Firefox/78.0"

// Crawler extracts fact-checks from snopes.com
type Crawler struct {
	name        string
	downloader  *ubur.Downloader
	IdleTimeout time.Duration
	Headers     map[string]string
}

// NewCrawler creates a new crawler and returns it
func NewCrawler(downloader *ubur.Downloader, name string) *Crawler {
	return &Crawler{
		name:        name,
		downloader:  downloader,
		IdleTimeout: time.Duration(time.Second * 120),
		Headers: map[string]string{
			"User-Agent": userAgent,
		},
	}
}

// Name returns the name of the crawler
func (crw *Crawler) Name() string {
	return crw.name
}

// Crawl start the crawling process
func (crw *Crawler) Crawl(ctx context.Context, wg *sync.WaitGroup) <-chan interface{} {
	results := make(chan interface{})
	go func() {
		defer close(results)
		rf := &ubur.RequestsFactory{
			Method: "GET",
			Header: crw.Headers,
		}

		// -- INDEX PAGE --
		firstURL, err := url.Parse(startURL)
		if err != nil {
			log.Fatal("Not a valid URL: ", startURL)
		}

		startURLs := make(chan *url.URL)
		articleURLs := make(chan *url.URL)

		s1 := ubur.NewBasicScheduler(crw.downloader.Concurrency, "INDEX")
		startRequests := rf.RequestsFromStream(ctx, wg, startURLs, "INDEX")

		go s1.Append(ctx, startRequests)
		go func() {
			select {
			case <-ctx.Done():
			case startURLs <- firstURL:
			}
		}()

		indexResults := crw.indexCrawl(ctx, wg, s1)
		crw.processIndexResults(ctx, wg, indexResults, startURLs, articleURLs)

		s2 := ubur.NewBasicScheduler(crw.downloader.Concurrency, "ARTICLE")

		articleRequests := rf.RequestsFromStream(ctx, wg, articleURLs, "ARTICLE")
		go s2.Append(ctx, articleRequests)

		articles := crw.articleCrawl(ctx, wg, s2)

		for {
			select {
			case <-ctx.Done():
				return
			case r, ok := <-articles:
				if !ok {
					return
				}
				select {
				case <-ctx.Done():
				case results <- r:
				}
			}
		}

	}()
	return results
}

// Parsers

type articleURL *url.URL
type nextPageURL *url.URL

// parseIndex is the parse method for the main page
// it extracts and add the article and page URLs to results
func parseIndex(ctx context.Context, r *http.Response) <-chan interface{} {
	results := make(chan interface{})
	go func() {
		defer close(results)
		defer r.Body.Close()
		doc, err := goquery.NewDocumentFromReader(r.Body)
		if err != nil {
			log.Printf("PARSER [INDEX]: error found => %+v\n", errors.Wrap(err, "Parser Error"))
			return
		}
		doc.Find("main article a.stretched-link").Each(
			func(_ int, s *goquery.Selection) {
				link, exists := s.Attr("href")
				if !exists {
					err = fmt.Errorf("Link does not have attribute href: %v", s)
					log.Printf("PARSER [INDEX]: error found => %+v", errors.Wrap(err, "Parser Error"))
					return
				}
				linkURL, err := url.Parse(link)
				if err != nil {
					log.Printf("PARSER [INDEX]: error found => %+v\n", errors.Wrap(err, "Parser Error"))
					return
				}
				select {
				case <-ctx.Done():
					return
				case results <- articleURL(r.Request.URL.ResolveReference(linkURL)):
				}
			})
		p := doc.Find("ul.pagination a:contains(Next)").First()
		if p.Length() > 0 {
			link, exists := p.Attr("href")
			if !exists {
				err = fmt.Errorf("Next Page does not have attribute href: %v", p)
				log.Printf("PARSER [INDEX]: error found => %+v", errors.Wrap(err, "Parser Error"))
				return
			}
			linkURL, err := url.Parse(link)
			if err != nil {
				log.Printf("PARSER [INDEX]: error found => %+v\n", errors.Wrap(err, "Parser Error"))
				return
			}
			select {
			case <-ctx.Done():
				return
			case results <- nextPageURL(r.Request.URL.ResolveReference(linkURL)):
			}
		}
	}()
	return results
}

// parseArticle parse the article data from the article page
// add a new Article item to results
func parseArticle(ctx context.Context, r *http.Response) <-chan interface{} {
	results := make(chan interface{})
	go func() {
		defer close(results)
		defer r.Body.Close()
		idregex := regexp.MustCompile(`.*p=(\d+)$`)
		dateregex := regexp.MustCompile(`.*?(\d{1,2} \w+ \d{4}).*`)
		doc, err := goquery.NewDocumentFromReader(r.Body)
		if err != nil {
			log.Printf("PARSER [ARTICLE]: error found => %+v\n", errors.Wrap(err, "Parser Error"))
			return
		}

		sel := doc.Selection

		published := dateregex.FindStringSubmatch(sel.Find(".dates").Text())
		claim := strings.TrimSpace(sel.Find(".claim-text").Text())

		if len(published) < 2 && claim == "" {
			log.Printf("No article found in %s\n", r.Request.URL.String())
			return
		}

		identifier := idregex.FindStringSubmatch(sel.Find("link[rel=shortlink]").AttrOr("href", ""))
		title := strings.TrimSpace(sel.Find("header h1").First().Text())

		if len(identifier) < 2 {
			log.Printf("No identifier found in %s\n", r.Request.URL.String())
			return
		}

		if title == "" {
			log.Printf("Unable to parse the title in %s\n", r.Request.URL.String())
			return
		}

		tags := make([]string, 0)

		sel.Find(".tags a").Each(func(_ int, s *goquery.Selection) {
			tags = append(tags, strings.TrimSpace(s.Text()))
		})

		sources := make([]string, 0)

		sel.Find(".citations .list-group-item").Each(func(_ int, s *goquery.Selection) {
			sourceList := make([]string, 0)
			for _, st := range strings.Fields(strings.TrimSpace(s.Find("p").Text())) {
				st = strings.TrimSpace(st)
				if st != "" {
					sourceList = append(sourceList, st)
				}
			}
			sourcetext := strings.Join(sourceList, " ")
			sourcetext = strings.TrimSpace(sourcetext)
			sources = append(sources, sourcetext)
		})

		article := items.Article{}
		article.Identifier = identifier[1]
		article.URL = r.Request.URL.String()
		article.Title = title
		article.Author = strings.TrimSpace(sel.Find("header .authors li a").First().Text())
		if len(published) > 1 {
			article.PublishedDate = published[1]
		}
		article.Claim = claim
		article.Rating = strings.TrimSpace(sel.Find(`.media-body span[class*="rating-label"]`).Text())
		article.Tags = tags
		article.Sources = sources

		select {
		case <-ctx.Done():
			return
		case results <- &article:
		}

	}()
	return results
}

// Crawling process

func (crw *Crawler) indexCrawl(
	ctx context.Context,
	c *sync.WaitGroup,
	s ubur.Scheduler) <-chan interface{} {
	indexResponses := crw.downloader.Download(ctx, c, s, "INDEX")
	return ubur.Parse(ctx, c, parseIndex, indexResponses, "INDEX")
}

func (crw *Crawler) articleCrawl(
	ctx context.Context,
	c *sync.WaitGroup,
	s ubur.Scheduler) <-chan interface{} {
	articleResponses := crw.downloader.Download(ctx, c, s, "ARTICLES")
	return ubur.Parse(ctx, c, parseArticle, articleResponses, "ARTICLES")
}

func (crw *Crawler) processIndexResults(ctx context.Context, c *sync.WaitGroup,
	results <-chan interface{}, indexURLs chan *url.URL, articleURLs chan *url.URL) {
	c.Add(1)
	go func() {
		defer c.Done()
		defer close(indexURLs)
		defer close(articleURLs)
		for {
			idle, done := context.WithTimeout(ctx, crw.IdleTimeout)
			select {
			case <-ctx.Done():
				done()
				log.Printf("INDEX PROCESSOR: canceling - reason: %+v\n", ctx.Err())
				return
			case <-idle.Done():
				done()
				return
			case r, ok := <-results:
				if !ok {
					log.Println("INDEX PROCESSOR: done")
					done()
					return
				}
				switch r.(type) {
				case articleURL:
					select {
					case articleURLs <- r.(articleURL):
					case <-ctx.Done():
					}
				case nextPageURL:
					select {
					case indexURLs <- r.(nextPageURL):
					case <-ctx.Done():
					}
				}
				done()
			}
		}
	}()
}

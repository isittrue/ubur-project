package politifact

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"net/url"
	"regexp"
	"strings"
	"sync"
	"time"

	"github.com/PuerkitoBio/goquery"
	"github.com/pkg/errors"
	"gitlab.com/merfrei/isittrue/ubur-project/items"
	"gitlab.com/merfrei/isittrue/ubur-project/pkg/selectors"
	ubur "gitlab.com/merfrei/ubur/crawler"
)

const indexURL = "https://www.politifact.com/factchecks/"
const userAgent = "Mozilla/5.0 (X11; Linux x86_64; rv:78.0) Gecko/20100101 Firefox/78.0"

// Crawler is a crawler to parse articles from politifact.com
type Crawler struct {
	name        string
	downloader  *ubur.Downloader
	IdleTimeout time.Duration
	Headers     map[string]string
}

// NewCrawler creates a new crawler and returns it
func NewCrawler(downloader *ubur.Downloader, name string) *Crawler {
	return &Crawler{
		name:        name,
		downloader:  downloader,
		IdleTimeout: time.Duration(time.Second * 120),
		Headers: map[string]string{
			"User-Agent": userAgent,
		},
	}
}

// Name returns the crawler name
func (crw *Crawler) Name() string {
	return crw.name
}

// Crawl starts the crawling process and returns a channel with results
func (crw *Crawler) Crawl(ctx context.Context, wg *sync.WaitGroup) <-chan interface{} {
	results := make(chan interface{})
	go func() {
		defer close(results)
		rf := &ubur.RequestsFactory{
			Method: "GET",
			Header: crw.Headers,
		}

		// -- INDEX PAGE --
		startURL, err := url.Parse(indexURL)
		if err != nil {
			log.Fatal("Not a valid URL: ", indexURL)
		}

		indexURLs := make(chan *url.URL)
		articleURLs := make(chan *url.URL)

		s1 := ubur.NewBasicScheduler(crw.downloader.Concurrency, "INDEX")
		indexRequests := rf.RequestsFromStream(ctx, wg, indexURLs, "INDEX")
		go s1.Append(ctx, indexRequests)
		go func() {
			select {
			case <-ctx.Done():
			case indexURLs <- startURL:
			}
		}()
		indexResults := crw.indexCrawl(ctx, wg, s1)
		crw.processIndexResults(ctx, wg, indexResults, indexURLs, articleURLs)

		s2 := ubur.NewBasicScheduler(crw.downloader.Concurrency, "ARTICLE")

		articleRequests := rf.RequestsFromStream(ctx, wg, articleURLs, "ARTICLE")
		go s2.Append(ctx, articleRequests)

		articles := crw.articleCrawl(ctx, wg, s2)

		for {
			select {
			case <-ctx.Done():
				return
			case r, ok := <-articles:
				if !ok {
					return
				}
				select {
				case <-ctx.Done():
				case results <- r:
				}
			}
		}
	}()
	return results
}

// Parsers

type articleURL *url.URL
type nextPageURL *url.URL

// parseIndex is the parse method for the main page
// it extracts and add the article and page URLs to results
func parseIndex(ctx context.Context, r *http.Response) <-chan interface{} {
	resultS := make(chan interface{})
	go func() {
		defer close(resultS)
		defer r.Body.Close()
		doc, err := goquery.NewDocumentFromReader(r.Body)
		if err != nil {
			log.Printf("PARSER [INDEX]: error found => %+v\n", errors.Wrap(err, "Parser Error"))
			return
		}
		doc.Find(".m-statement__quote a[href]").Each(
			func(_ int, s *goquery.Selection) {
				link, exists := s.Attr("href")
				if !exists {
					err = fmt.Errorf("Link does not have attribute href: %v", s)
					log.Printf("PARSER [INDEX]: error found => %+v", errors.Wrap(err, "Parser Error"))
					return
				}
				linkURL, err := url.Parse(link)
				if err != nil {
					log.Printf("PARSER [INDEX]: error found => %+v\n", errors.Wrap(err, "Parser Error"))
					return
				}
				select {
				case <-ctx.Done():
					return
				case resultS <- articleURL(r.Request.URL.ResolveReference(linkURL)):
				}
			})
		doc.Find("a.c-button.c-button--hollow:contains(Next)").Each(
			func(_ int, p *goquery.Selection) {
				link, exists := p.Attr("href")
				if !exists {
					err = fmt.Errorf("Next Page does not have attribute href: %v", p)
					log.Printf("PARSER [INDEX]: error found => %+v", errors.Wrap(err, "Parser Error"))
					return
				}
				linkURL, err := url.Parse(link)
				if err != nil {
					log.Printf("PARSER [INDEX]: error found => %+v\n", errors.Wrap(err, "Parser Error"))
					return
				}
				select {
				case <-ctx.Done():
					return
				case resultS <- nextPageURL(r.Request.URL.ResolveReference(linkURL)):
				}
			})
	}()
	return resultS
}

// parseArticle parse the article data from the article page
// add a new Article item to results
func parseArticle(ctx context.Context, r *http.Response) <-chan interface{} {
	resultS := make(chan interface{})
	go func() {
		defer close(resultS)
		defer r.Body.Close()
		sdre := regexp.MustCompile(`\s(\w+\s\d+,\s\d+)\s`)
		doc, err := goquery.NewDocumentFromReader(r.Body)
		if err != nil {
			log.Printf("PARSER [ARTICLE]: error found => %+v\n", errors.Wrap(err, "Parser Error"))
			return
		}
		doc.Find("main").Each(
			func(_ int, s *goquery.Selection) {
				url := r.Request.URL.String()
				splits := strings.Split(url, "/")
				var identifier string
				for i := len(splits) - 1; i >= 0; i-- {
					if splits[i] != "" {
						identifier = splits[i]
						break
					}
				}
				article := items.Article{
					Identifier:    identifier,
					URL:           url,
					Title:         strings.TrimSpace(s.Find("h2.c-title").Text()),
					Author:        strings.TrimSpace(s.Find(".m-author__content > a").Text()),
					PublishedDate: strings.TrimSpace(s.Find(".m-author__date").First().Text()),
					Claim:         strings.TrimSpace(s.Find(".m-statement__quote-wrap > .m-statement__quote").First().Text()),
					Rating:        selectors.FindAttr(s, ".m-statement__meter .c-image__original", "alt"),
					Tags:          []string{},
					Sources:       []string{},
				}
				dateStr := s.Find(".m-statement__desc").Text()
				matches := sdre.FindStringSubmatch(dateStr)
				if len(matches) > 1 {
					article.ClaimDate = matches[1]
				}
				s.Find("ul.m-list.m-list--horizontal a.c-tag").Each(func(_ int, a *goquery.Selection) {
					tagName := strings.TrimSpace(a.Find("span").Text())
					if tagName != "" {
						article.Tags = append(article.Tags, tagName)
					}
				})
				s.Find("#sources article p").Each(func(_ int, p *goquery.Selection) {
					sourceText := strings.TrimSpace(p.Text())
					if sourceText != "" {
						article.Sources = append(article.Sources, sourceText)
					}
				})

				select {
				case <-ctx.Done():
					return
				case resultS <- &article:
				}
			})
	}()
	return resultS
}

// Crawling process

func (crw *Crawler) indexCrawl(
	ctx context.Context,
	c *sync.WaitGroup,
	s ubur.Scheduler) <-chan interface{} {
	indexResponses := crw.downloader.Download(ctx, c, s, "INDEX")
	return ubur.Parse(ctx, c, parseIndex, indexResponses, "INDEX")
}

func (crw *Crawler) articleCrawl(
	ctx context.Context,
	c *sync.WaitGroup,
	s ubur.Scheduler) <-chan interface{} {
	articleResponses := crw.downloader.Download(ctx, c, s, "ARTICLES")
	return ubur.Parse(ctx, c, parseArticle, articleResponses, "ARTICLES")
}

func (crw *Crawler) processIndexResults(ctx context.Context, c *sync.WaitGroup,
	results <-chan interface{}, indexURLs chan *url.URL, articleURLs chan *url.URL) {
	c.Add(1)
	go func() {
		defer c.Done()
		defer close(indexURLs)
		defer close(articleURLs)
		for {
			idle, done := context.WithTimeout(ctx, crw.IdleTimeout)
			select {
			case <-ctx.Done():
				done()
				log.Printf("INDEX PROCESSOR: canceling - reason: %+v\n", ctx.Err())
				return
			case <-idle.Done():
				done()
				return
			case r, ok := <-results:
				if !ok {
					log.Println("INDEX PROCESSOR: done")
					done()
					return
				}
				switch r.(type) {
				case articleURL:
					select {
					case articleURLs <- r.(articleURL):
					case <-ctx.Done():
					}
				case nextPageURL:
					select {
					case indexURLs <- r.(nextPageURL):
					case <-ctx.Done():
					}
				}
				done()
			}
		}
	}()
}

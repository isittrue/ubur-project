package crawlers

import (
	"context"
	"sync"
)

// Crawler is the interface to implement by any crawler
type Crawler interface {
	Name() string
	Crawl(context.Context, *sync.WaitGroup) <-chan interface{}
}

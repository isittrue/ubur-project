package mongo

import (
	"context"
	"sync"

	"gitlab.com/merfrei/isittrue/ubur-project/pkg/crawlers"
	"gitlab.com/merfrei/ubur/pipelines/importer"
	"go.mongodb.org/mongo-driver/mongo"

	ubur "gitlab.com/merfrei/ubur/pipelines/importer"
)

// Importer will import the results into MongoDB
type Importer struct {
	db *mongo.Database
}

// NewMongoImporter create a new importer
func NewMongoImporter(db *mongo.Database) *Importer {
	return &Importer{
		db: db,
	}
}

// ImportCrawlResults will import the crawl results into mongo
// the collection name will be the crawler name
func (imp *Importer) ImportCrawlResults(ctx context.Context, wg *sync.WaitGroup, crw crawlers.Crawler) {
	col := imp.db.Collection(crw.Name())
	results := crw.Crawl(ctx, wg)
	uburMongoImporter := ubur.NewMongoImporter(col, nil)
	importer.Pipeline(ctx, wg, uburMongoImporter, results)
}

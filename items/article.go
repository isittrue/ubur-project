package items

// Article extracted
type Article struct {
	Identifier    string   `json:"identifier"`
	URL           string   `json:"url"`
	Title         string   `json:"title"`
	Author        string   `json:"author"`
	PublishedDate string   `json:"publishedDate"`
	Claim         string   `json:"claim"`
	ClaimDate     string   `json:"claimDate"`
	Rating        string   `json:"rating"`
	Tags          []string `json:"tags"`
	Sources       []string `json:"sources"`
	Status        string   `json:"status"`
}

// GetIdentifier returns the identifier of the article
func (art *Article) GetIdentifier() string {
	return art.Identifier
}

// GetT returns a reference to a new Article
func (art *Article) GetT() interface{} {
	return &Article{}
}

// SetStatus set a new status for the article
func (art *Article) SetStatus(status string) {
	art.Status = status
}

package crawler

import (
	"bytes"
	"context"
	"log"
	"net/http"
	"net/url"
	"sync"
	"time"

	"github.com/pkg/errors"
)

const defaultTimeout = time.Duration(180 * time.Second)

// Downloader makes the requests and write the responses to a channel
type Downloader struct {
	client      *http.Client
	ProxyFunc   func(*http.Request) (*url.URL, error)
	Timeout     time.Duration
	MaxTry      uint
	Concurrency uint
	Delay       uint
}

// NewDownloader creates and returns a new Downloader reference
func NewDownloader(maxTry uint, concurrency uint) *Downloader {
	return &Downloader{
		MaxTry:      maxTry,
		Concurrency: concurrency,
	}
}

// Init will initalize the downloader
// it could be explicitely called or calling Download directly
// if the downloader is not initialized yet it will be done there
func (d *Downloader) Init() {
	timeout := d.Timeout
	if timeout == 0 {
		timeout = defaultTimeout
	}
	if d.ProxyFunc != nil {
		d.client = &http.Client{
			Transport: &http.Transport{
				Proxy:                 d.ProxyFunc,
				DisableKeepAlives:     true,
				ResponseHeaderTimeout: timeout,
			},
		}
	} else {
		d.client = &http.Client{
			Transport: &http.Transport{
				ResponseHeaderTimeout: timeout,
			},
		}
	}
}

// ShouldRetry check if a request should be retried
func (d *Downloader) ShouldRetry(r *Request) bool {
	if d.MaxTry == 0 {
		return false
	}
	return r.TryNo < d.MaxTry
}

// Download process a requests stream and return a new responses stream
func (d *Downloader) Download(ctx context.Context, wg *sync.WaitGroup,
	s Scheduler, desc string) <-chan *http.Response {
	if d.client == nil {
		d.Init()
	}
	responsesStream := make(chan *http.Response)
	wg.Add(1)
	go func() {
		defer wg.Done()
		defer close(responsesStream)

		var rwg sync.WaitGroup
		spots := make(chan int, d.Concurrency)
		for {
			select {
			case <-ctx.Done():
				log.Printf("DOWNLOADER [%s]: canceling - reason: %v\n", desc, ctx.Err())
				rwg.Wait()
				return
			case r := <-s.Requests():
				spots <- 1
				rwg.Add(1)
				go func() {
					defer rwg.Done()
					defer func() {
						<-spots
					}()
					resp, err := d.MakeRequest(r)
					if d.Delay > 0 {
						time.Sleep(time.Second * time.Duration(d.Delay))
					}
					if err != nil {
						log.Printf("DOWNLOADER [%s]: error found => %+v",
							desc, errors.Wrap(err, "Downloader Error"))
						if d.ShouldRetry(r) {
							log.Printf("DOWNLOADER [%s]: retrying %s, failed %d times",
								desc, r.URL, r.TryNo)
							go s.Schedule(ctx, r)
						} else {
							log.Printf("DOWNLOADER [%s]: give up retrying for %s, max try reached: %d",
								desc, r.URL, d.MaxTry)
						}
						return
					}
					select {
					case <-ctx.Done():
					case responsesStream <- resp:
					}
				}()
			default:
				// Check if the scheduler is Done. Only when there is
				// no more requests to consume at the moment
				select {
				case <-ctx.Done():
				case <-s.Done():
					log.Printf("DOWNLOADER [%s]: done\n", desc)
					rwg.Wait()
					return
				default:
				}
			}
		}
	}()
	return responsesStream
}

// MakeRequest performs a new request and returns the response or an error
func (d *Downloader) MakeRequest(r *Request) (*http.Response, error) {
	var err error
	var req *http.Request

	if r.IsForm {
		r.Body = bytes.NewBufferString(r.FormData.Encode())
	}

	req, err = http.NewRequest(r.Method, r.URL, r.Body)
	if err != nil {
		return nil, err
	}

	if r.IsForm {
		req.Header.Set("Content-Type", "application/x-www-form-urlencoded; param=value")
	}

	for hk, hv := range r.Header {
		req.Header.Set(hk, hv)
	}

	for ck, cv := range r.Cookies {
		req.AddCookie(&http.Cookie{Name: ck, Value: cv})
	}

	r.TryNo++

	return d.client.Do(req)
}

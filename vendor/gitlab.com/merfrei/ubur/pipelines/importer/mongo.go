package importer

import (
	"context"
	"fmt"
	"log"
	"sync"

	"github.com/pkg/errors"
	"gitlab.com/merfrei/ubur/items"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

// MongoImporter is an implementation of the Importer for a MongoDB database
type MongoImporter struct {
	collection *mongo.Collection
	opts       *options.FindOneAndUpdateOptions
}

// NewMongoImporter create and return a new MongoImporter instance
func NewMongoImporter(collection *mongo.Collection, opts *options.FindOneAndUpdateOptions) *MongoImporter {
	if opts == nil {
		opts = options.FindOneAndUpdate().SetUpsert(true) // UPSERT is the default
	}
	return &MongoImporter{
		collection: collection,
		opts:       opts,
	}
}

// ImportItems will import the items in a MongoDB database
func (imp *MongoImporter) ImportItems(ctx context.Context, wg *sync.WaitGroup, itemsChan <-chan interface{}) <-chan interface{} {
	importedItems := make(chan interface{})

	wg.Add(1)
	go func() {
		defer wg.Done()
		defer close(importedItems)
	loop:
		for {
			select {
			case <-ctx.Done():
				log.Printf("IMPORTER: canceling - reason: %+v", ctx.Err())
				return
			case item, ok := <-itemsChan:
				if !ok {
					log.Println("IMPORTER: done")
					return
				}
				itemI, iok := item.(items.Item)
				if !iok {
					log.Printf("IMPORTER: error found => %+v",
						errors.Wrap(fmt.Errorf("Item is not a valid item interface %v", item), "Importer Error"))
					continue loop
				}
				filter := bson.D{{Key: "identifier", Value: itemI.GetIdentifier()}}
				update := bson.D{{Key: "$set", Value: item}}
				newItem := itemI.GetT()
				err := imp.collection.FindOneAndUpdate(ctx, filter, update, imp.opts).Decode(newItem)
				if err != nil {
					if err == mongo.ErrNoDocuments {
						itemI.SetStatus("new")
					} else {
						log.Printf("IMPORTER: unexpected error found => %+v",
							errors.Wrap(err, "Importer Unexpected Error"))
					}
				} else {
					itemI.SetStatus("updated")
				}
				select {
				case <-ctx.Done():
					log.Printf("IMPORTER: canceling - reason: %+v", ctx.Err())
					return
				case importedItems <- item:
				}
			}
		}
	}()

	return importedItems
}

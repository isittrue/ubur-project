package main

import (
	"context"
	"flag"
	"io/ioutil"
	"log"
	"os"
	"os/signal"
	"sync"
	"syscall"
	"time"

	"gitlab.com/merfrei/isittrue/ubur-project/pkg/crawlers/politifact"
	importer "gitlab.com/merfrei/isittrue/ubur-project/pkg/pipelines/importer"
	ubur "gitlab.com/merfrei/ubur/crawler"
	"gitlab.com/merfrei/ubur/pkg/config"
	mongoM "gitlab.com/merfrei/ubur/pkg/mongo"
	"gitlab.com/merfrei/ubur/pkg/pservice"
)

const maxTry = 10

var concurrency uint
var timeout = time.Duration(time.Second * 30)

func main() {
	configFile := flag.String("config", "config.toml", "Path to the config file")
	crwName := flag.String("name", "politifact", "The crawler name")
	psTarget := flag.String("pstarget", "politifact.com", "The Proxy Target identifier")
	flag.UintVar(&concurrency, "concurrency", 20, "The concurrency value to use")

	flag.Parse()

	// DEBUG
	// go utils.DebugGR(time.Second * 10)

	ctx, stop := signal.NotifyContext(context.Background(), os.Interrupt, syscall.SIGTERM)
	defer stop()

	configD, err := ioutil.ReadFile(*configFile)
	if err != nil {
		log.Fatal(err)
	}
	config := config.Load(configD)

	mongoCli, err := mongoM.Connect(ctx, config.Mongo.URI)
	if err != nil {
		log.Fatal(err)
	}
	defer mongoM.Disconnect(ctx, mongoCli)

	db := mongoCli.Database(config.Mongo.Database)

	// Proxy Service enabled
	ps := pservice.New(config.ProxyService.URL, config.ProxyService.APIKey, *psTarget)
	err = ps.Start(ctx, make(map[string]string))
	if err != nil {
		log.Fatal(err)
	}

	downloader := ubur.NewDownloader(maxTry, concurrency)
	downloader.Delay = 2
	downloader.ProxyFunc = ps.ProxyFunc
	downloader.Timeout = timeout

	crawler := politifact.NewCrawler(downloader, *crwName)

	var wg sync.WaitGroup

	imp := importer.NewMongoImporter(db)
	imp.ImportCrawlResults(ctx, &wg, crawler)

	wg.Wait()
}

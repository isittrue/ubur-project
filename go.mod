module gitlab.com/merfrei/isittrue/ubur-project

go 1.16

require (
	github.com/PuerkitoBio/goquery v1.6.1
	github.com/pkg/errors v0.9.1
	gitlab.com/merfrei/ubur v0.2.4
	go.mongodb.org/mongo-driver v1.4.6
)
